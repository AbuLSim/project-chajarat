#!/bin/bash
echo "Starting installation..."
# Tried and works with python3.6 and python3.9
PYTHON=python3.6

# Python Pip packages
$(PYTHON) -m pip install virtualenv
if [ ! -d "django-env" ]; then
  echo "Creating a virtual environement"
  $(PYTHON) -m venv django-env
fi
wget https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-linux64.tar.gz
tar -xvzf geckodriver*
chmod +x geckodriver
mv geckodriver django-env/
rm geckodriver-v0.24.0-linux64.tar.gz
source django-env/bin/activate
$(PYTHON) -m pip install --upgrade pip
echo "Installing required packages in virtual environement"
$(PYTHON) -m pip install -r requirements.txt
export PATH=$PATH:~/project-chajarat/django-env/geckodriver
deactivate
