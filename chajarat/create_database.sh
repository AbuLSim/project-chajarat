#!/bin/bash
python3.6 manage.py makemigrations
python3.6 manage.py migrate


echo "Starting Scraping..."
export PYTHONWARNINGS="ignore:Unverified HTTPS request"
python3.6 scraping/plantonspourlavenir.py
#python3.6 scraping/atreeforyou.py
#python3.6 scraping/my-tree.py
export PATH=$PATH:../django-env
export PATH=$PATH:../django-env
python3.6 scraping/reforestaction.py
export PATH=$PATH:../django-env
python3.6 scraping/treenation.py

rm scraping/geckodriver.log
