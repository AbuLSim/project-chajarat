import os, sys
import django


currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", 'chajarat.settings')
django.setup()

from map.models import Departement

# Fix the PB of the departement nb
import csv

def make_dpt_dict():
    dpt_dict = {}
    with open(currentdir+'/departement.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)

        for row in reader:
            dpt = Departement(
                name = row['nom_departement'],
                nb = row['code_departement'],
            )
            dpt.save()
            dpt_dict[row['nom_departement']] = dpt
            dpt_dict[row['code_departement']] = dpt
    return dpt_dict
