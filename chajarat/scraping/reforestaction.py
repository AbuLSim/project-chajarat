from selenium import webdriver
from requests import get
import time
import re
import json

import os, sys
import django
import communesFilter.filter as filter


from dpt_field import make_dpt_dict

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)



import os

os.environ['PATH'] += os.path.dirname(parentdir) + '/django-env'

os.environ.setdefault("DJANGO_SETTINGS_MODULE", 'chajarat.settings')
django.setup()
from map.models import Project


def produceCorrectLinks():
    links = filter.createLinks()
    projects = 1
    i = 0
    while i < len(links):
        if projects > 2 :
            i -= 1
            links[i] = links[i].replace("-"+str(projects-1)+"-","-"+str(projects)+"-")
        website_url = "https://www.reforestaction.com/" + links[i]
        response = get(website_url)
        if response.status_code == 404:
            if projects == 1:
                i+=1
            projects = 1
        else :
            print (website_url)
            projects +=1
        i+=1


def scrape(driver, url):
    dpt_dict = make_dpt_dict()
    print("\tScraping "+url[:-1]+"...",end='',flush=True)
    driver.get(url)
    time.sleep(2)

    title = driver.find_element_by_class_name('intro-text').text


    boxmain = driver.find_element_by_class_name('box-main')
    nbArbresText = boxmain.find_elements_by_tag_name('p')[0].text
    patterns = ["(\d+) (\d+) (\d+)","(\d+) (\d+)","(\d+)"]
    cnt = 3
    for i in patterns:
        nbArbres = ""
        out = re.search(i,nbArbresText)
        if out != None:
            for j in range(cnt):
                nbArbres += out.group(j+1)
            break
        cnt -= 1
    nbArbres = int(nbArbres)

    # Check if project contains trees
    if nbArbres == 0:
        print(" 0 trees planted: no project created")
        return
    print("")

    # Description finding
    description = driver.find_elements_by_class_name('field-item')
    if description != [] :
        description = description[0].find_elements_by_tag_name('p')
        if len(description)<=0 :
            description = ""
        else:
            if len(description)>1 :
                description = description[1].text
            else :
                description = description[0].text
    else:
        description = ""
    # City and Departement from link
    city = boxmain.find_element_by_tag_name('h3').text
    out = re.search(r'(.+) \((\d+)\)',city)
    city = out.group(1)
    departement = out.group(2)

    # if No title
    if title == "":
        title = city
    # image_url
    img = driver.find_elements_by_class_name('col-sm-4')
    found = False
    for img1 in img :
        img2 = img1.find_elements_by_class_name('slick-slide')
        if img2 != []:
            for img3 in img2:
                img4 = img3.find_elements_by_class_name('slick-active')
                if img4 !=[]:
                    found == True
                    img = img4
                    break
            if found == True:
                break

    if img != []:
        img = img[0].find_element_by_tag_name('img')
        img_url = img.get_attribute('src')
        # found a wrong url
        if img_url.find("logo")>=0:
            img_url = ""

    # Localisation
    url_api_loc = "https://api-adresse.data.gouv.fr/search/?q="
    response = get(url_api_loc+city)
    jsonfile = json.loads(response.text)
    geom = jsonfile['features'][0]['geometry']
    # distanciate two projects in same place
    projNum =  re.search(r'-(\d+)-',url)
    if projNum != None:
        projNum = int(projNum.group(1))/50
        geom['coordinates'][0] += projNum
        geom['coordinates'][1] += projNum
    #   localisation
    if img_url != "":
        Project(
            title = title,
            description = description,
            picture_url = img_url,
            url = url,
            geom = geom,
            city = city,
            departement = dpt_dict[departement],
            nbArbres = nbArbres,
        ).save()

def scrapeAll():
    print("Scraping https://www.reforestaction.com")
    driver = webdriver.Firefox()
    file = open(currentdir+'/reforestactionlinks.txt', 'r')

    # dummy read to prevent future redirection to english link
    driver.get("https://www.reforestaction.com/le-chautay-18")
    while True:
        link = file.readline()
        if link.find("https")<0:
            break
        scrape(driver,link)
    file.close()
    driver.quit()

scrapeAll()
