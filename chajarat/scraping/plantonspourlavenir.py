from requests import get
from bs4 import BeautifulSoup
import re
import json
import os, sys
import django


currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", 'chajarat.settings')
django.setup()
from map.models import Project

from dpt_field import make_dpt_dict

def scrape():
	dpt_dict = make_dpt_dict()
	url = 'https://www.plantonspourlavenir.fr/les-projets-planter'
	url_api_loc = "https://api-adresse.data.gouv.fr/search/?q="
	print("Scraping "+url+ "...")

	response = get(url,verify=False)
	html_soup = BeautifulSoup(response.text, 'html.parser')
	projects = html_soup.find_all('div', class_ = 'col-md-4 projet')


	# getting pages
	pages = []
	url_pictures = []
	for pj in projects:
		pages.append(pj.find('a')['href'])
		picture = pj.find('img')['src']
		url_pictures.append(picture)

	it = 0
	for page in pages:
		print("\tScraping "+page)
		response = get(page,verify=False)
		html_soup = BeautifulSoup(response.text, 'html.parser')

		data = html_soup.find('div',class_="contenu")
		txt = data.find('h1').text
		outer = re.compile(r".+ la commune d[e']\s*(.+)\s\(([0-9]+)\)")
		m = outer.search(txt.strip())
		city = m.group(1)
		departement = m.group(2)
		title = data.find('h2').text
		outer = re.compile(r"Le projet : (.+)")
		m = outer.search(title)
		title = m.group(1)


		description = data.find_all('p')[0].text
		picture_url	 = url_pictures[it]
		nbArbres = html_soup.find('span',class_="nbr green").text

		response = get(url_api_loc+city)
		jsonfile = json.loads(response.text)
		geom = jsonfile['features'][0]['geometry']
		it += 1
		Project(
			title = title,
			description = description,
			picture_url = picture_url,
			url = page,
			geom = geom,
			city = city,
	        nbArbres = int(nbArbres.replace(' ','')),
			departement = dpt_dict[departement],
		).save()


scrape()
