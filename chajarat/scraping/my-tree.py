from requests import get
from bs4 import BeautifulSoup
from django.utils import encoding
import os, sys
import django


currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", 'chajarat.settings')
django.setup()



from map.models import Project

from scraping.dpt_field import make_dpt_dict



website_url = "https://www.my-tree.com"
projects_url = website_url + "/fr/all-projects"
# Projects Urls assemble

def collect_url():
    response = get(projects_url)
    html_soup = BeautifulSoup(response.text, 'html.parser')
    a = html_soup.find_all('a', class_='btn btn-full btn-full--white')
    urls = []
    for i in a :
        urls.append(i['href'])
    return urls

def twoDecimal(f):
    return float(int(f * 100))/100

def scrape():
    dpt_dict = make_dpt_dict()
    projects_num = collect_url()
    print ("Scraping " + website_url)
    for num in projects_num:
        url = website_url + num
        print("\tScraping " + url + "...")
        response = get(url)
        html_soup = BeautifulSoup(response.text, 'html.parser')

        # Localisation
        scripts = html_soup.find_all('script', type='text/javascript', src='')
        location_script = scripts[2].string
        indexStart = location_script.find('[[')
        stringLatLong = location_script[indexStart+2:indexStart+30]
        latitude = stringLatLong[0:6] #17 characters for lat
        indexComma = stringLatLong.find(',')
        longitude = stringLatLong[indexComma+1:indexComma+7]

        latitude = twoDecimal(float(latitude))
        longitude = twoDecimal(float(longitude))

        # Picture URL
        img_url = html_soup.find_all('section', class_="header big")[0]['style']
        img_url = img_url.rpartition('url(')[2]
        img_url = img_url[:len(img_url)-2]
        img_url = website_url+ img_url

        # Description
        description = html_soup.find_all('div', id="page-content-project")[0]
        description = description.find_all('p')
        par = ""
        cnt = 0
        for p in description:
            cnt += 1
            if p.text == ' ' or cnt == 2:
                break
            par += p.text + " "
        # Title
        title = html_soup.find_all('div', class_="col-sm-12")[0].h2.text
        title=title.replace("  ", "")
        if title[0] == ' ' :
            title = title[1:]

        # city
        city = html_soup.find_all('div', class_="stat--description")[1].p.text.strip()
        if (city == 'Seine-Maritme') : city = 'Seine-Maritime'

        # numberPlants
        nbArbres = html_soup.find_all('div', class_="stat--title-number")[0].text

        Project(
            title = title,
            description = par,
            picture_url = img_url,
            url = url,
            geom = {'type': 'Point', 'coordinates': [latitude, longitude]},
            city = city,
            departement = dpt_dict[city],
            nbArbres = int(nbArbres.replace(' ','')),
        ).save()

scrape()
