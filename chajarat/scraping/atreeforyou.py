from requests import get
from bs4 import BeautifulSoup
import re
import json

import os,sys
import django




currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", 'chajarat.settings')
django.setup()

from map.models import Project
from dpt_field import make_dpt_dict



def scrape():
	dpt_dict = make_dpt_dict()
	url_api_loc = "https://api-adresse.data.gouv.fr/search/?q="
	urls = ['https://www.atreeforyou.org/fr/nos-plantations/',\
		'https://www.atreeforyou.org/fr/nos-plantations/?product-page=2',\
		'https://www.atreeforyou.org/fr/nos-plantations/?product-page=3',\
		'https://www.atreeforyou.org/fr/nos-plantations/?product-page=4']

	print ("Scraping https://www.atreeforyou.org/")
	for url in urls:
		print ("\tScraping "+ url)

		response = get(url)
		html_soup = BeautifulSoup(response.text, 'html.parser')
		projects_details = html_soup.find_all('div',class_="product-details")
		projects_image = html_soup.find_all('div',class_="kleo-woo-image")
		project_descriptions = html_soup.find_all('div',class_="woocommerce-product-details__short-description")
		nb = html_soup.find_all('span', id=re.compile('^funded_product_'),class_="atfy_i18_number")
		assert len(project_descriptions) == len(projects_details)

		# Two pictures for one project : discard one
		picture_urls_temp = []
		picture_urls = []
		nbArbres = []
		for pjim in projects_image:
			picture_url = pjim.find('img')['src']
			picture_urls_temp.append(picture_url)
#			nbArbres += [nb.replace(" ","")]
		pair = 0
		picture_urls = picture_urls_temp[::2]
		assert len(picture_urls) == len(projects_details)


		it = -1
		for pj in projects_details:
			it += 1
			title = pj.text
			url = pj.find('a')['href']
			#find france in title
			if (url.find('france') != -1):
				picture_url = picture_urls[it]
				description = project_descriptions[it].text
				nbArbres = nb[it].text

				# finding localisation from title
				outer = re.compile(r"(.+) en France, (.+)")
				motif = outer.search(title)
				if (motif == None):
					outer = re.compile(r"(.+), en (.+), FRANCE")
					motif = outer.search(title)
					if (motif == None):
						outer = re.compile(r"(.+), en (.+), France")
						motif = outer.search(title)
						if (motif == None):
							outer = re.compile(r"(.+), (.+), France")
							motif = outer.search(title)
							if (motif == None):
								outer = re.compile(r"(.+) à (.+), France")
								motif = outer.search(title)

				if (motif != None):
					title = motif.group(1)
					localisation = motif.group(2).strip()
					# finding latitude longitude from government database
					response = get(url_api_loc +localisation)
					jsonfile = json.loads(response.text)
					geom = jsonfile['features'][0]['geometry']
					if (localisation == 'Normandie'): localisation ='Argentan'
					if (not localisation in dpt_dict):
						outer = re.compile(r"(.+), (.+), (.+)")
						response = get(url_api_loc +localisation)
						jsonfile = json.loads(response.text)
						dpt = jsonfile['features'][0]['properties']['context']
						motif = outer.search(dpt)
						localisation = motif.group(2)
					Project(
						title = title,
						description = description,
						picture_url = "https://www.sgsgroup.fr/-/media/global/images/structural-website-images/hero-images/hero-agri-forestry.jpg",
						url = url,
						geom = geom,
						city = localisation,
						departement = dpt_dict[localisation],
						nbArbres = int(nbArbres.replace(' ','')),
					).save()



scrape()
