import os,sys
import django

from selenium import webdriver
import re
import time
from requests import get
import json

from dpt_field import make_dpt_dict

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

import os

os.environ['PATH'] += os.path.dirname(parentdir) + '/django-env'
os.environ.setdefault("DJANGO_SETTINGS_MODULE", 'chajarat.settings')
django.setup()
from map.models import Project


def cleanURLpicture(urlstr):
	index = urlstr[1:].find("http")
	if index != -1 :
		urlstr = urlstr [index+1:]
	return urlstr

def scrape():
	dpt_dict = make_dpt_dict()
	url_api_loc = "https://api-adresse.data.gouv.fr/search/?q="
	url = "https://tree-nation.com/projects"
	print("Scraping " + url+"...")
	driver = webdriver.Firefox()
	driver.get(url)
	pages = []
	titles_project = []

	button = driver.find_element_by_class_name('load-more-results')
	for i in range(12):
		time.sleep(5)
		button.click()

	projects = driver.find_elements_by_class_name('project-prev1')
	for pj in projects:
		if (pj.find_element_by_css_selector('h3.project-prev1__name').find_element_by_tag_name('a').text[-6:] == "France"):
			pages.append(pj.find_element_by_tag_name('a').get_property('href')[:-7] + "sites#headers")

	urls = []
	picture_urls = []
	cities = []
	descriptions = []
	geoms = []
	titles = []
	nbArbreList =[]

	for page in pages:
		driver.get(page)
		time.sleep(5)
		projects = driver.find_elements_by_class_name('planting-sites')
		for pj in projects:
			time.sleep(5)
			titles_container = pj.find_elements_by_tag_name('h3')
			for title_c in titles_container:
				title = title_c.text
				outer = re.compile(r"(.+)\s\(([0-9]+)\)")
				m = outer.search(title)
				if (m != None):
					title = m.group(1)
					city = m.group(1)
				else:
					city = title
				compiles=[re.compile(r"Plantation au (.*)"),\
						re.compile(r"Plantation à (.*)"),\
						re.compile(r"Plantations à (.*)"),\
						re.compile(r"Reboisement de l'aéroport de (.*)"),\
						re.compile(r"Des haies à (.*)"),\
						re.compile(r"Plantation à (.*)"),\
						re.compile(r"Plantation (.*)")]
				for cmp in compiles :
					m = cmp.search(city)
					if (m != None):
						city = m.group(1)
						break
						city = m.group(1)
				cities.append(city)
				titles.append(title)
			picture_urls_container = pj.find_elements_by_tag_name('source')
			pair = 0
			for picture_url_container in picture_urls_container:
				if pair == 0:
					picture_urls.append(picture_url_container.get_attribute('srcset'))
					pair = 1
				else:
					pair = 0

			urls_container = pj.find_elements_by_class_name('planting-site__details-link')
			for url_container in urls_container:
				url = url_container.get_attribute('href')
				urls.append(url)



	for url in urls:
		print("\tScraping " + url)
		time.sleep(5)
		driver.get(url)
		description = driver.find_element_by_class_name('description').text[:-13]
		patterns = ['arbres (\d+)', '(\d+) arbres',\
					'arbre (\d+)', '(\d+) arbre',\
					'arbustes (\d+)', '(\d+) arbustes',\
					'arbuste (\d+)', '(\d+) arbuste',\
					'arbres (\d+) (\d+)','(\d+) (\d+) arbres',\
					'arbre (\d+) (\d+)','(\d+) (\d+) arbre',\
					'arbuste (\d+) (\d+)','(\d+) (\d+) arbuste',\
					'arbustes (\d+) (\d+)','(\d+) (\d+) arbustes']

		found = False
		for p in range(len(patterns)):
			nbArbres = re.search(patterns[p],description,re.IGNORECASE)
			if nbArbres != None:
				if p<len(patterns)/2:
					nbArbres = nbArbres.group(1)
				else:
					nbArbres = nbArbres.group(1) + nbArbres.group(2)
				found = True
				break

		if found == False:
			nbArbres = driver.find_elements_by_class_name('pr-header__stat-value--featured')[1]
			nbArbres =nbArbres.text
		nbArbres = int(nbArbres.replace(" ",""))
		nbArbreList += [nbArbres]
		descriptions.append(description)


	# getting latitude longitude
	for city in cities:
		response = get(url_api_loc+city)
		jsonfile = json.loads(response.text)
		geom = jsonfile['features'][0]['geometry']
		geoms.append(geom)

	driver.quit()

	for i in range(len(cities)):
		outer = re.compile(r"(.+), (.+), (.+)")
		response = get(url_api_loc + cities[i])
		jsonfile = json.loads(response.text)
		dpt = jsonfile['features'][0]['properties']['context']
		motif = outer.search(dpt)
		departement = motif.group(1)
		if nbArbreList[i] !=0:
		    Project(
		            title = titles[i],
		            description = descriptions[i],
		            picture_url = cleanURLpicture(picture_urls[i]),
		            url = urls[i],
		            geom = geoms[i],
		            city = cities[i],
					nbArbres = nbArbreList[i],
					departement=dpt_dict[departement],
		        ).save()

scrape()
