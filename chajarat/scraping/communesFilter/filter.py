import csv

import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))



# reading communes
def createLinks():
    links = []
    with open(currentdir+'/communes-departement-region.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader :
            if row == []:
                break
            if line_count == 0:
                line_count += 1
            else:
                line_count += 1
                name = row[1]
                if name[0] == 'L' and name[1]== ' ':
                    name = name[2:]
                name=name.replace(" STE "," SAINTE ")
                if name[0:4] == "STE ":
                    name = "SAINTE " + name[4:]
                name=name.replace(" ST "," SAINT ")
                if name[0:3] == "ST ":
                    name = "SAINT " + name[3:]
                name = name.lower()
                name = name.replace(" ","-")
                departement = row[11]
                if len(departement) == 1:
                    departement = '0'+departement
                links += [name+'-'+departement]
                links += [name+'-2-'+departement]
    return links

def findLocalisation(city):
    with open(currentdir+'/communes-departement-region.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader :
            if row == []:
                break
            if line_count == 0:
                line_count += 1
            else:
                line_count += 1
                name = row[1]
