from djgeojson.fields import PointField
from django.db import models
import re


class Project(models.Model):

    title = models.CharField(max_length=200)
    description = models.TextField()
    picture_url = models.URLField(max_length=200)
    url = models.URLField(max_length=200)
    geom = PointField()
    city = models.CharField(max_length=50)
    departement = models.ForeignKey('Departement',on_delete=models.CASCADE)
    nbArbres = models.PositiveIntegerField(default=1)

    def __str__(self):
        return self.title

    # Metadata
    class Meta:
        ordering = ['title']

    @property
    def website(self):
        outer = re.compile(r"(.*)my-tree(.*)")
        m = outer.search(self.url)
        if (m != None):
            return "my-tree"
        outer = re.compile(r"(.*)atreeforyou(.*)")
        m = outer.search(self.url)
        if (m != None):
            return "atreeforyou"
        outer = re.compile(r"(.*)plantonspourlavenir(.*)")
        m = outer.search(self.url)
        if (m != None):
            return "plantonspourlavenir"
        return "Null"


class Departement(models.Model):
    nb = models.PositiveIntegerField(default=0)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name
