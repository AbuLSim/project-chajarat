from django.shortcuts import render

from django.shortcuts import render

from map.models import Project
from map.models import Departement
from django.db.models import Sum
from django.views import generic

import csv
import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

def map(request):
    labels = []
    data = []

    queryset = Project.objects.values('departement').annotate(Sum('nbArbres'))[:10]
    for pj in queryset:
        labels.append(Departement.objects.get(id=pj['departement']).name)
        data.append(pj['nbArbres__sum'])

    return render(request, 'index.html', {
        'labels': labels,
        'data': data,
    })


def departementListView(request):
    template_name = 'project_list.html'
    # Read all departements
    departements = []

    filedir = parentdir + '/scraping'
    with open(filedir+'/departement.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader :
            if row == []:
                break
            if line_count == 0:
                line_count += 1
            else:
                line_count += 1
                departements += [{ 'departement': row[1], 'nb' : int(row[0]) }]
    return render(request, template_name, context={'departements': departements})


def departementDetailView(request,pk):
    departements = Departement.objects.all()
    template_name = 'departement_detail.html'
    projects = Project.objects.filter(departement__nb=pk)
    nb_trees_planted = projects.aggregate(Sum('nbArbres'))['nbArbres__sum']
    nb_projects = projects.count()
    return render(request, template_name, context={'project_list': projects, 'nb_trees_planted':nb_trees_planted,'nb_projects':nb_projects})
