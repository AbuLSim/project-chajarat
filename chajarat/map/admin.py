from leaflet.admin import LeafletGeoAdmin
from django.contrib import admin

from .models import Project

admin.site.register(Project, LeafletGeoAdmin)

class ProjectAdmin(LeafletGeoAdmin):
    list_display = ('title', 'geom')

