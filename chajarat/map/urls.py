from django.views.generic import TemplateView
from django.conf.urls import url
from djgeojson.views import GeoJSONLayerView
from .models import Project
from . import views
from django.views.generic.base import RedirectView
from django.contrib.staticfiles.storage import staticfiles_storage
from django.urls import path

urlpatterns = [
     url(r'^$', TemplateView.as_view(template_name='index.html'), name='map'),
     url(r'^data$', GeoJSONLayerView.as_view(model=Project, properties=('title', 'description', 'picture_url','url','website')), name='data'),
     path('favicon.ico', RedirectView.as_view(url=staticfiles_storage.url('favicon.ico'))),
     path('projects/', views.departementListView, name='projects'),
     path('departement/<int:pk>', views.departementDetailView, name='departement'),

]
