# Chajarat
Let's ACT for nature!
## About the Project
**Chajarat** (tree in Arabic)
is a donation-based website for
tree planting in the World. It aggregates
data about different websites which propose
to plant trees such as the price, the place of the future tree,
the species of the tree and build a new map with all aggregated
information. Then a donor can choose a project on the
map in order to participate to it. The aim
is to reduce the pollution in
the world by encouraging the people
to help planting trees.

## Existing Websites for planting Tree
We collect our data from several websites in order to propose
a large choice of projects. The list of websites is:
* https://www.atreeforyou.org
* https://tree-nation.com
* https://www.plantonspourlavenir.fr
* https://www.my-tree.com
* https://www.reforestaction.com

The locations, project description and races
info are extracted. All the above sites give these informations.
Data extraction is done by using automated and manual web scraping techniques.

## Installing Tools
The project installation assumes the user has Python 3.6.
and Firefox web browser (Without firefox **part** of the data
will not be possible to scrape and an error will occur during
the scraping phase). 

If python3.6 is not installed then run
```sh
sudo apt-get install python3.6
sudo apt-get install python3-pip
```
To install the tools run the following command
```sh
chmod +x install.sh ; ./install.sh
```
## Launching Project
Activate the virtual environment that was created during installation
```sh
source django-env/bin/activate
```
Then lunch the server with

```sh
cd chajarat
python3.6 manage.py makemigrations
python3.6 manage.py migrate
python3.6 manage.py loaddata db.json  --exclude contenttypes
python3.6 manage.py runserver
```
Open your web browser on http://127.0.0.1:8000
